import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    this.itemsMap = itemsMap;
  }

  get itemDetails () {
    const itemDetails = dishes.filter(dish => this.itemsMap.has(dish.id)).map(item =>
      ({
        ...item,
        count: this.itemsMap.get(item.id)
      }));
    return itemDetails;
  }

  get totalPrice () {
    return this.itemDetails.map(item => item.price * item.count).reduce((prev, next) => prev + next);
  }
}

export default Order;
