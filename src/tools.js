function parseInputToMap (selectedItems) {
  return new Map(selectedItems.split(',').map(item =>
    [item.split(' x ')[0], parseInt(item[item.length - 1])]));
}

export { parseInputToMap };
